package main

import (
	"context"
	firebase "firebase.google.com/go"
	"flag"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/thebeardedone/push-notification-service/api"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"
)

func main() {

	logrus.SetFormatter(&logrus.JSONFormatter{})

	port := flag.Int("port", 3002, "port the server is listening on")
	flag.Parse()

	if *port < 0 || *port > 65535 {
		logrus.WithFields(logrus.Fields{
			"port": *port,
		}).Error("invalid port")
		os.Exit(1)
	}

	firebaseApp, firebaseInitError := firebase.NewApp(context.Background(), nil)
	if firebaseInitError != nil {
		logrus.WithFields(logrus.Fields{
			"cause": firebaseInitError,
		}).Error("failed to initialize firebase")
		os.Exit(1)
	}

	router := mux.NewRouter()
	router.HandleFunc("/api/send", api.Send(firebaseApp, "send")).Methods(http.MethodPost)

	server := Start(*port, router)
	<-WaitForStopSignal()
	Stop(server)
}

func Start(port int, router *mux.Router) http.Server {

	server := http.Server{
		Addr:         ":" + strconv.Itoa(port),
		Handler:      router,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}

	logrus.WithFields(logrus.Fields{
		"version": getVersion(),
		"commit": GitCommit,
		"branch": GitBranch,
		"state": GitState,
		"built": BuildDate,
	}).Info("starting server")
	go func() {
		logrus.Panic(server.ListenAndServe())
	}()
	logrus.WithFields(logrus.Fields{
		"port": port,
	}).Info("server is running")

	return server
}

func WaitForStopSignal() chan os.Signal {
	stop := make(chan os.Signal)
	signal.Notify(stop, os.Interrupt, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL, syscall.SIGQUIT)
	return stop
}

func Stop(server http.Server) {
	logrus.Info("shutting down server")

	if err := server.Shutdown(context.Background()); err != nil {
		logrus.Error("failed to gracefully shutdown server")
	}

	logrus.Info("gracefully shutdown server")
}