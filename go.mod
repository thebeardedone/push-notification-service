module gitlab.com/thebeardedone/push-notification-service

go 1.13

require (
	cloud.google.com/go/firestore v1.2.0 // indirect
	firebase.google.com/go v3.12.0+incompatible
	github.com/ahmetb/govvv v0.3.0
	github.com/gorilla/mux v1.7.4
	github.com/sirupsen/logrus v1.5.0
)
