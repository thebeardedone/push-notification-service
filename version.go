package main

var (
	Version	   string
	BuildDate  string
	GitCommit  string
	GitBranch  string
	GitState   string
)

func getVersion() string {
	if Version == "" {
		return "dev"
	} else {
		return Version
	}
}