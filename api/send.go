package api

import (
	"context"
	"encoding/json"
	"firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"github.com/sirupsen/logrus"
	"gitlab.com/thebeardedone/push-notification-service/utils"
	"net/http"
)

func Send(firebaseApp *firebase.App, route string) http.HandlerFunc {
	return func(writer http.ResponseWriter, reader *http.Request) {

		correlationId := reader.Header.Get("X-Correlation-ID")

		ctx := context.Background()
		client, clientRetrievalError := firebaseApp.Messaging(ctx)
		if clientRetrievalError != nil {
			errorMessage := "failed to retrieve firebase client"
			logrus.WithFields(logrus.Fields{
				"cause": clientRetrievalError,
				"correlationId": correlationId,
				"route": route,
			}).Error(errorMessage)
			utils.OnError(writer, http.StatusInternalServerError, errorMessage)
			return
		}

		pushNotificationRequest := utils.PushNotificationRequest{}
		decodeError := json.NewDecoder(reader.Body).Decode(&pushNotificationRequest)
		defer reader.Body.Close()
		if decodeError != nil {
			errorMessage := "failed to decode push notification request"
			logrus.WithFields(logrus.Fields{
				"cause": decodeError,
				"correlationId": correlationId,
				"route": route,
			}).Error(errorMessage)
			utils.OnError(writer, http.StatusBadRequest, errorMessage)
			return
		}

		message := &messaging.MulticastMessage{
			Notification: &messaging.Notification{
				Title:    pushNotificationRequest.Title,
				Body:     pushNotificationRequest.Body,
				ImageURL: pushNotificationRequest.Image,
			},
			Data: pushNotificationRequest.Data,
			Tokens: pushNotificationRequest.Tokens,
		}

		response, multicastError := client.SendMulticast(ctx, message)
		if multicastError != nil {
			errorMessage := "failed to send push notifications"
			logrus.WithFields(logrus.Fields{
				"cause": multicastError,
				"correlationId": correlationId,
				"route": route,
			}).Error(errorMessage)
			utils.OnError(writer, http.StatusInternalServerError, errorMessage + ". Cause: " + multicastError.Error())
			return
		}

		var failedTokens []string
		if response.FailureCount > 0 {
			for idx, resp := range response.Responses {
				if !resp.Success {
					// The order of responses corresponds to the order of the registration tokens.
					failedTokens = append(failedTokens, pushNotificationRequest.Tokens[idx])
				}
			}
		}

		logrus.WithFields(logrus.Fields{
			"correlationId": correlationId,
			"failedTokenCount": response.FailureCount,
			"route": route,
		}).Info("successfully processed push notification request")

		utils.OnSuccess(writer, failedTokens)
	}
}