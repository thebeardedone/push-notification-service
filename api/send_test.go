package api

import (
	"bytes"
	"encoding/json"
	"github.com/sirupsen/logrus"
	"gitlab.com/thebeardedone/push-notification-service/utils"
	"net/http"
	"testing"
	"time"
)

func TestSend(tester *testing.T) {

	// replace with your own server url
	serverUrl := "http://localhost:3002/api/send"

	pushNotificationRequest := utils.PushNotificationRequest{
		Title:  "Hello World",
		Body:   "You have received a push notification!",
		Tokens: []string{
			// replace with you own firebase client token.
			"",
		},
	}

	jsonObject, marshalError := json.Marshal(pushNotificationRequest)
	if marshalError != nil {
		logrus.WithFields(logrus.Fields{
			"cause": marshalError,
		}).Error("failed to marshal push notification request")
		tester.FailNow()
	}

	client := http.Client{
		Timeout: time.Second * 2,
	}

	request, requestGenerationError := http.NewRequest(http.MethodPost, serverUrl,
		bytes.NewBufferString(string(jsonObject)))
	if requestGenerationError != nil {
		logrus.WithFields(logrus.Fields{
			"cause": requestGenerationError,
		}).Error("failed to create post request")
		tester.FailNow()
	}
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("X-Correlation-ID", "test")
	response, postError := client.Do(request)

	if postError != nil {
		logrus.WithFields(logrus.Fields{
			"cause": postError,
		}).Error("failed to send post request")
		tester.FailNow()
	}

	if response.StatusCode != http.StatusOK {
		var data map[string]string
		if decodeError := json.NewDecoder(response.Body).Decode(&data); decodeError != nil {
			logrus.WithFields(logrus.Fields{
				"cause": decodeError,
			}).Error("failed to decode post response error message")
			tester.FailNow()
		}

		logrus.WithFields(logrus.Fields{
			"cause": data,
		}).Error("post request failed")
		tester.FailNow()
	}

	var failedTokens []string
	if decodeError := json.NewDecoder(response.Body).Decode(&failedTokens); decodeError != nil {
		logrus.WithFields(logrus.Fields{
			"cause": decodeError,
		}).Error("failed to decode post response")
		tester.FailNow()
	}

	logrus.WithFields(logrus.Fields{
		"failedTokens": failedTokens,
		"status": response.StatusCode,
	}).Info("successfully received response")
}