# Push Notification Service

A standalone firebase push notification server implementation providing a JSON API endpoint to send push notifications.

## Standalone

### Compiling

Run go build or with govvv for version information.

```
go build -ldflags="$(govvv -flags) -X main.Version=1.0.0"
```

### How to run

Set the environment variable for the service account and run the server:

```
export GOOGLE_APPLICATION_CREDENTIALS="/home/user/Downloads/service-account-file.json"
./push-notification-service
```

By default the server listens on 3002, if you wish to listen on another port, use the -port switch.

### Sample request

Now you'll be able to send a request to `http://<server_ip>:<port>/api/send` in order to send push notifications.
The api expects a request with the content-type `application/json` and the following payload:

```
{
    "title":"Hello World",
    "body":"You have received a push notification!",
    "image": "",
    "data": "",
    "tokens":[
        // TODO: fill in your client tokens here
        ""
    ]
}
```

A correlation id may be added for debugging purposes by setting the `X-Correlation-ID` header.

## Docker

### Compiling

The docker image in this repository uses Alpine linux and requires the binary to be built with `CGO_ENABLED=0`:

```
CGO_ENABLED=0 go build -ldflags="$(govvv -flags) -X main.Version=1.0.0"
```

### Building image

From the root directory, run the following command:

```
docker build -t push-notification-service -f docker/Dockerfile .
```

### Running images

You will have to mount a volume containing your firebase configuration file and specify it's name in the environment variable.

```
docker run -it -v <path_containing_config>:/conf/ -e GOOGLE_APPLICATION_CREDENTIALS="/conf/<config_filename>" push-notification-service
```