package utils

type PushNotificationRequest struct {
	Title		string				`json:"title,omitempty"`
	Body		string				`json:"body,omitempty"`
	Image		string				`json:"image,omitempty"`
	Data		map[string]string	`json:"data,omitempty"`
	Tokens		[]string			`json:"tokens,omitempty"`
}