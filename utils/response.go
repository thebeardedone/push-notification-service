package utils

import (
	"encoding/json"
	"net/http"
)

func OnSuccess(writer http.ResponseWriter, payload interface{}) {
	SendJSONResponse(writer, http.StatusOK, payload)
}

func OnError(writer http.ResponseWriter, code int, message string) {
	SendJSONResponse(writer, code, map[string]string{"error": message})
}

func SendJSONResponse(writer http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(code)
	writer.Write(response)
}